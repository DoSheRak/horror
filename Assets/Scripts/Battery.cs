﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    /*void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            FindObjectOfType<Flashlight>().AddBattery();
            Destroy(gameObject);
        }
    }*/

    GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void UseBattery()
    {
        player.GetComponentInChildren<Flashlight>().AddBattery();
        Destroy(gameObject);
    }

    void Update()
    {
        
    }
}
