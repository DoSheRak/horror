﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    GameObject woodBox;
    void Start()
    {
        woodBox = GameObject.FindGameObjectWithTag("Respawn");
        woodBox.SetActive(false);
    }

    public void UseKey()
    {
        woodBox.SetActive(true);
        Destroy(gameObject);
    }

    void Update()
    {
        
    }
}
