﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flashlight : MonoBehaviour
{
    Light spotlight;

    public float currentBattery;
    float maxBattery = 100;
    public float lightDrain = 1f; // в 1 секунду

    public Image batteryUI;

    void Start()
    {
        spotlight = GetComponent<Light>();
        currentBattery = maxBattery;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (spotlight.enabled == false)
            {
                spotlight.enabled = true;
            }
            else
            {
                spotlight.enabled = false;
            }
        }

        if (currentBattery < 0)
        {
            spotlight.enabled = false;
        }

        if (spotlight.enabled == true && currentBattery > 0)
        {
            currentBattery -= lightDrain * Time.deltaTime;
        }

        batteryUI.transform.localScale = new Vector3(currentBattery/maxBattery, batteryUI.transform.localScale.y, batteryUI.transform.localScale.z);

    }

    public void AddBattery()
    {
        currentBattery = maxBattery;
    }

}
