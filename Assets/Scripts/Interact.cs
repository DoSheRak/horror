﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Interact : MonoBehaviour
{
    public float interactDistance;
    public LayerMask interactLayer;

    public Image interactIcon;

    void Start()
    {
        if (interactIcon != null)
        {
            interactIcon.enabled = false;
        }
    }

    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit; //объект, с которым столкнулись

        if (Physics.Raycast(ray, out hit, interactDistance, interactLayer))
        {
            if (interactIcon != null)
            {
                interactIcon.enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.collider.tag == "Battery")
                {
                    //FindObjectOfType<Flashlight>().AddBattery();
                    //Destroy(GameObject.FindGameObjectWithTag("Battery"));
                    hit.collider.GetComponent<Battery>().UseBattery();
                }

                if (hit.collider.tag == "Finish")
                {
                    hit.collider.GetComponent<Key>().UseKey();
                }

                if (hit.collider.tag == "Respawn")
                {
                    SceneManager.LoadScene("Victory");
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }

                if (hit.collider.tag == "Note")
                {
                    hit.collider.GetComponent<Note>().UseNote();
                }
            }
        }
        else
        {
            interactIcon.enabled = false;
        }
    }
}
