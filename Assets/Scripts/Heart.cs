﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Heart : MonoBehaviour
{
    public float currentXP;
    float maxXP = 100;
    public float xpDrain = 20f;

    public Image xpUI;
    GameObject player;
    GameObject enemy;

    void Start()
    {
        currentXP = maxXP;
        player = GameObject.FindGameObjectWithTag("Player");
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    void Update()
    {
        xpUI.transform.localScale = new Vector3(currentXP / maxXP, xpUI.transform.localScale.y, xpUI.transform.localScale.z);

        if (currentXP < 0)
        {
            SceneManager.LoadScene("GameOver");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void Damage()
    {
        currentXP -= xpDrain;
    }
}
