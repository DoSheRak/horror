﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Monster : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //GameObject.Find("Heart").GetComponent<Heart>().Damage();
            FindObjectOfType<Heart>().Damage();
            //FindObjectOfType<Flashlight>().AddBattery();
            //Destroy(gameObject);
        }
    }

    GameObject player;
    NavMeshAgent navigator;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        navigator = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        navigator.destination = player.transform.position;
    }
}
