﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Food : MonoBehaviour
{
    public float currentFood;
    float maxFood = 100;
    public float foodDrain = 1f;

    public Image foodUI;

    void Start()
    {
        currentFood = maxFood;
    }

    void Update()
    {
        if (currentFood > 0)
        {
            currentFood -= foodDrain * Time.deltaTime;
        }

        foodUI.transform.localScale = new Vector3(currentFood / maxFood, foodUI.transform.localScale.y, foodUI.transform.localScale.z);

        if (currentFood < 0)
        {
            SceneManager.LoadScene("GameOver");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
