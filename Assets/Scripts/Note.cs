﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Note : MonoBehaviour
{
    public Image note;
    public Text text;
    GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        note.enabled = false;
        text.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            note.enabled = false;
            text.enabled = false;
            Destroy(gameObject);
        }
        
        if (Input.GetKeyDown(KeyCode.X))
        {
            note.enabled = false;
            text.enabled = false;
            Destroy(gameObject);
        }
    }

    public void UseNote()
    {
        note.enabled = true;
        text.enabled = true;
    }
}
