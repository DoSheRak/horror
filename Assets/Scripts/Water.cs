﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Water : MonoBehaviour
{
    public float currentWater;
    float maxWater = 100;
    public float waterDrain = 2f;

    public Image waterUI;

    void Start()
    {
        currentWater = maxWater;
    }

    void Update()
    {
        if (currentWater > 0)
        {
            currentWater -= waterDrain * Time.deltaTime;
        }

        waterUI.transform.localScale = new Vector3(currentWater / maxWater, waterUI.transform.localScale.y, waterUI.transform.localScale.z);

        if (currentWater < 0)
        {
            SceneManager.LoadScene("GameOver");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
